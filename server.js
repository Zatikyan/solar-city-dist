const express = require('express')
const fs = require('fs')
const app = express()
    const port = process.env.OPENSHIFT_NODEJS_PORT || 8080;
    const ip = process.env.IP || process.env.OPENSHIFT_NODEJS_IP || '0.0.0.0';

    const index = fs.readFileSync('index.html', 'utf8')

    app.get('/', (req, res) => res.send(index))

    app.listen(port, ip, () => console.log(`Example app listening on port ${port}!`))

    app.use(express.static('.'))
